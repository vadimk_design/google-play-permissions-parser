from django.conf.urls import url

from api_v1.views.Applications import get_app_permissions


urlpatterns = [
    url(r'^permissions/?$', get_app_permissions),
]
