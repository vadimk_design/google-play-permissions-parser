from rest_framework import serializers

from scrapper.models import Application, Permissions


class ApplicationMixin(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = ('gid',)


class PermissionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Permissions
        fields = ('title', 'description')


class ApplicationENSerializer(ApplicationMixin):
    title = serializers.CharField(source='en_title')
    permissions = PermissionsSerializer(many=True, source='en_permissions')

    class Meta:
        model = Application
        fields = ApplicationMixin.Meta.fields + ('title', 'permissions')


class ApplicationRUSerializer(ApplicationMixin):
    title = serializers.CharField(source='ru_title')
    permissions = PermissionsSerializer(many=True, source='ru_permissions')

    class Meta:
        model = Application
        fields = ApplicationMixin.Meta.fields + ('title', 'permissions')
