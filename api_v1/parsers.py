import requests
import json

from scrapper.models import Application, Permissions


BASE_URL = 'https://play.google.com/store/xhr/getdoc?authuser=0'


def parse_permissions(item):
    """ Вспомогательная функция для разбора разрешений
    """
    return {'title': item[0], 'description': item[1]}


def parse_google_response(data):
    j = json.loads(data.text[5:])

    app_title = j[0][-1][0][8]

    permissions = j[0][-1][0][-1]['42656262'][1]

    # добавить парсинг групп разрешений

    # создаём список со всеми разрешениями приложения
    app_permissions = []
    for permission in permissions:
        if permission:  # отсеиваем пустые списки
            for item in permission:
                # выбираем разрешения по длине списка
                if item[0] is None:
                    for _item in item[1]:
                        app_permissions.append(parse_permissions(_item))
                else:
                    app_permissions.append(parse_permissions(item))

    return app_title, app_permissions


def app_parser(app_id):
    DATA_RU = [
        ('ids', app_id),
        ('xhr', '1'),
        ('hl', 'ru')
    ]
    DATA_EN = [
        ('ids', app_id),
        ('xhr', '1'),
        ('hl', 'en')
    ]

    app_page_data_ru = requests.post(BASE_URL, data=DATA_RU)
    app_page_data_en = requests.post(BASE_URL, data=DATA_EN)

    if app_page_data_ru.status_code == requests.codes.ok:
        ru_title, ru_permissions = parse_google_response(app_page_data_ru)

    if app_page_data_en.status_code == requests.codes.ok:
        en_title, en_permissions = parse_google_response(app_page_data_en)

    app = Application(
        gid=app_id,
        en_title=en_title,
        ru_title=ru_title
    )
    app.save()

    en_permissions_ids = []
    for perm in en_permissions:
        permission, c = Permissions.objects.get_or_create(
            title=perm['title'],
            description=perm['description'],
            hl='en'
        )
        en_permissions_ids.append(permission.pk)

    ru_permissions_ids = []
    for perm in ru_permissions:
        permission, c = Permissions.objects.get_or_create(
            title=perm['title'],
            description=perm['description'],
            hl='ru'
        )
        ru_permissions_ids.append(permission.pk)

    app.en_permissions.add(*en_permissions_ids)
    app.ru_permissions.add(*ru_permissions_ids)
    app.save()

    return app.pk
