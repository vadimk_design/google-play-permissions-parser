from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.core.exceptions import ValidationError

from scrapper.models import Application
from api_v1.serializers.Applications import ApplicationENSerializer, ApplicationRUSerializer
from api_v1.parsers import app_parser


@api_view(['GET'])
def get_app_permissions(request):
    locale = request.query_params.get('hl', 'en')
    app_id = request.query_params.get('app_id', None)

    if app_id:
        app_instance = Application.objects.filter(gid=app_id).first()
        if not app_instance:
            # парсим приложение и кладём в БД
            app_parser(app_id)

        # отдаём данные согласно указанному языку
        if locale == 'en':
            serializer = ApplicationENSerializer(Application.objects.get(gid=app_id))
        elif locale == 'ru':
            serializer = ApplicationRUSerializer(Application.objects.get(gid=app_id))

    else:
        raise ValidationError("You must enter an app id")

    return Response(serializer.data)
