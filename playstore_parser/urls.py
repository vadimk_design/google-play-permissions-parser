from django.conf.urls import url, include
from django.contrib import admin

from scrapper.views import IndexView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', IndexView.as_view()),
    url(r'^api/v1/', include('api_v1.urls', namespace='api_v1')),
]
