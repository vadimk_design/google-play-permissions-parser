from django.contrib import admin

from scrapper.models import Application, Permissions


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = ['ru_title', 'gid']


@admin.register(Permissions)
class PermissionsAdmin(admin.ModelAdmin):
    list_display = ['title']
