from django.db import models


class Application(models.Model):
    gid = models.CharField(max_length=255, verbose_name='ID приложения', null=False, unique=True)
    en_title = models.CharField(max_length=255, verbose_name='Название приложения (EN)', null=False)
    en_permissions = models.ManyToManyField('Permissions', limit_choices_to={'hl': 'en'},
                                            related_name='en_permissions', verbose_name='Разрешения (EN)')
    ru_title = models.CharField(max_length=255, verbose_name='Название приложения (RU)', null=False)
    ru_permissions = models.ManyToManyField('Permissions', limit_choices_to={'hl': 'ru'},
                                            related_name='ru_permissions', verbose_name='Разрешения (RU)')

    class Meta:
        verbose_name = 'Приложение'
        verbose_name_plural = 'Приложения'

    def __str__(self):
        return self.ru_title


class Permissions(models.Model):
    EN, RU = 'en', 'ru'
    LOCALE_CHOICES = (
        (EN, 'английский'),
        (RU, 'русский')
    )

    title = models.CharField(max_length=255, verbose_name='Название разрешения', null=False, blank=False)
    description = models.TextField()
    hl = models.CharField(max_length=2, verbose_name='Локаль', default=None,
                          choices=LOCALE_CHOICES, null=False, blank=False)

    class Meta:
        verbose_name = verbose_name_plural = 'Разрешения'

    def __str__(self):
        return self.title
