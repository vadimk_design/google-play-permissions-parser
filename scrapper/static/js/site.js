BASE_URL = "http://0.0.0.0:8080/api/v1";

// Обновление списков
function clear_info() {
    $("#app-info").empty();
    $("#permissions-list").empty();
}


function on_receive_api_root(data) {
    url = BASE_URL
    update_screen(BASE_URL);
}


$(document).ready(function () {
    $("#permissions-form").submit(function(e){
        e.preventDefault();
        clear_info();
        create_product(
          $("#appid").val(), $("#hl").val(),
          on_receive_permissions);
    });
});

function create_product(appid, hl, on_success) {

    var data = {
        "app_id": appid,
        "hl": hl
    };

    $.ajax({
        url: BASE_URL + "/permissions/",
        type: "GET",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: on_success,
        error: function(){
            alert("Произошла ошибка. Повторите попытку");
        }
    });

}


// Получение и обработка списка продуктов
function on_receive_permissions(data) {
    $("#app-info").append(
      $("<h2>" + data.title + "</h2><h3>" + data.gid + "</h3>")
    )
    $.each(data.permissions, function (i, permission) {
        on_get_permissions(permission);
    });
}


function on_get_permissions(permission) {
    $("#permissions-list").append(
      $("<li>"
        + "<p>" + permission.title + "</p>"
        + "<span>" + permission.description + "</span>"
      + "</li>")
    );
}
